
let num = +prompt("Enter number", "")
while (typeof num !== 'number' || Number.isNaN(num)) {
  num = +prompt("Enter number again", "")
}

function factorial(n) {
  if (n === 0) {
    return 1;
  }else {
    return n * factorial(n - 1);
  }
}
alert(factorial(num));
